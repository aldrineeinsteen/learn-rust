fn main() {
    println!("Hello, world!");

    let x = 1;
    let y = x;

    println!("The value of x is {}", x);

    let str_1 = String::from("Hello");
    let str_2 = str_1.clone(); //the reference is moved from str_1 to str_2 as String doesn't have copy trait.

    println!("The value of str_1 is {}", str_1);
}
