# Learn Rust
My own journey for learning Rust-Lang.
## Getting started
To execute any of the sub-modules, navigate to the said module.
### 1. Guessing Game
For running the project from code.
```shell
cd guessing-game
cargo run --release
```
### 2. Times Table
For running the project from code.
```shell
cd times-table
cargo run --release
```