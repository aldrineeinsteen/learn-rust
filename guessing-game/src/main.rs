use std::cmp::Ordering;
use std::io;
use rand::Rng;
use colored::Colorize;

fn main() {
    println!("Creating a secret number (between 0 & 100)...");

    let secret_number = rand::thread_rng()
        .gen_range(0..100);
    let mut guess_str: String;
    let mut attempt = 1;

    loop {
        println!("Guess the secret number (between 0 & 100): ");
        guess_str = String::new();
        //reading the input from user
        io::stdin()
            .read_line(&mut guess_str)
            .expect("Failed to read the guess");

        //making sure the entered string is a number
        let guess: u32 = match guess_str.trim()
            .parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        //comparing the number
        match guess.cmp(&secret_number) {
            Ordering::Equal => {
                println!(
                    "{}",
                    format!("Wow! you found it.")
                        .bold()
                        .green()
                );
                println!("Solved it in {} attempts", attempt);
                break;
            }
            Ordering::Less => println!(
                "{}",
                format!("Too Small...")
                    .bold()
                    .yellow()
            ),
            Ordering::Greater => println!(
                "{}",
                format!("Too Big...")
                    .bold()
                    .red()
            )
        }
        attempt = attempt + 1;
    }
}
