use std::borrow::Borrow;
use std::io;
use rand::Rng;
use colored::Colorize;

struct GameState {
    has_guessed: bool,
    selected_word: String,
    attempt: u32,
}

fn main() {
    let _dictionary = include_str!("data/dictionary.txt").split_whitespace();
    let mut game_state = GameState {
        has_guessed: false,
        selected_word: String::new(),
        attempt: 0,
    };
    print_rules();

    println!("Enter the length of the challenge string:");

    let mut str_length = String::new();
    io::stdin().read_line(&mut str_length)
        .expect("Failed to read length.");

    let length: u32 = match str_length.trim()
        .parse() {
        Ok(num) => num,
        Err(_) => std::process::exit(1),
    };

    // println!("Hello, world!");
    let mut collection: Vec<String> = Vec::new();
    let mut index = 0;
    for i in _dictionary {
        if i.len() == length as usize
            && !i.contains("-")
            && !i.contains(".")
            && !i.contains("'") {
            collection.insert(index, i.to_uppercase());
            index = index + 1;
        }
    }

    let random_index = rand::thread_rng()
        .gen_range(0..index);

    //let selected_str = collection.get(random_index).unwrap();
    game_state.selected_word = String::from(collection.get(random_index).unwrap());
    // println!("Selected string is {}", selected_str);
    collection.clear();

    for y in 0..(length + 1) {
        // println!("Value of y: {}", y);
        println!("Enter your guess:");
        let mut guess = String::new();
        io::stdin().read_line(&mut guess)
            .expect("Failed to read from user.");
        guess = guess.trim().to_uppercase();
        guess.truncate(length as usize);
        println!("Attempt {}/{}", y + 1, length + 1);
        for x in 0..guess.len() {
            //print!("{}", x);
            check_and_print(game_state.selected_word.borrow(), &guess, x);
        }
        println!();
        if (game_state.selected_word).eq_ignore_ascii_case(&guess) {
            game_state.attempt = y + 1;
            game_state.has_guessed = true;
            break;
        } else {
            println!("Your guess {} in incorrect.", guess);
        }
    }
    if game_state.has_guessed {
        println!("Amazing! You guess the answer {} in {} attempts.", game_state.selected_word, game_state.attempt);
    } else {
        println!("The challenge is {}. Note: you have exhausted all attempts", game_state.selected_word);
    }
}

fn check_and_print(selected_str: &String, guess: &String, x: usize) {
    let x_range = x..x+1;
    //println!("Current index: {}", x);
    //let x:u32 = x as u32;
    //moving to slice
    //if guess.chars().nth(x).unwrap() == selected_str.chars().nth(x).unwrap() {
    if &guess[x_range.clone()] == &selected_str[x_range.clone()] {
        print!("{}", format!("{}", &guess[x_range.clone()]).bold().green())
    } else {
        if selected_str.contains(&guess[x_range.clone()]) {
            print!("{}", format!("{}", &guess[x_range.clone()]).bold().yellow())
        } else {
            print!("{}", format!("{}", &guess[x_range.clone()]).white())
        }
    }
}

fn print_rules() {
    println!(
        "{}",
        format!("----------------------------------------------------------------------------------------------------------------\n\
| 1. Each guess must match a valid word                                                                        |\n\
| 2. If the letters match their position - it will be highlighted in GREEN                                     |\n\
| 3. If the letters are found in the word but doesn't match their position - it will be highlighted in YELLOW  |\n\
| 4. If the letters isn't found in the word; they are highlighted in WHITE                                     |\n\
----------------------------------------------------------------------------------------------------------------")
            .bold()
            .green()
    );
}
