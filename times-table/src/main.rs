extern crate core;

use std::borrow::Borrow;
use std::cmp::Ordering;
use std::io;
use std::ops::Div;
use std::time::Instant;
use rand::Rng;
use colored::Colorize;

#[derive(Clone, Copy)]
struct Question {
    num_1: u32,
    num_2: u32,
    answer: u32,
    provided_answer: u32,
    is_correct: bool,
}

struct Questions {
    qs: Vec<Question>,
}

fn main() {
    let now = Instant::now();
    let mut qs_collection = Questions { qs: vec![] };
    println!("How many questions: ");

    let mut total_questions_str = String::new();

    io::stdin()
        .read_line(&mut total_questions_str)
        .expect("Failed to read total no of questions input.");

    let total_questions: i32 = match total_questions_str.trim()
        .parse() {
        Ok(num) => num,
        Err(_) => std::process::exit(1),
    };

    println!("Generating {} no of questions.", total_questions);

    execute(&total_questions, &mut qs_collection);

    let elapsed = now.elapsed();

    println!("{}",
             format!("{}", print_line(70).bold().yellow())
    );

    println!("You took {}.{}s for completing the worksheet.",
             format!(
                 "{}",
                 elapsed.as_secs()
             ).bold().yellow(),
             format!(
                 "{}",
                 elapsed.subsec_millis()
             ).bold().yellow(),
    );

    let avg_time_taken = elapsed
        .div(total_questions as u32);
    println!("You took roughtly {}.{}s for each question.",
             format!(
                 "{}",
                 avg_time_taken.as_secs()
             ).bold().yellow(),
             format!(
                 "{}",
                 avg_time_taken.subsec_millis()
             ).bold().yellow()
    );

    println!("{}",
             format!("{}", print_line(70).bold().yellow())
    );

    for q in qs_collection.qs.iter() {
        println!("{} x {} = {}", q.num_1, q.num_2, q.answer);
    }
}

fn execute(total_questions: &i32, qs_collection: &mut Questions) {
    let mut loop_index = 0;
    let mut count_correct_answer = 0;
    while loop_index.lt(total_questions) {
        let mut question = genetate_question();
        // let multipliers = generate_multipliers();
        // println!("What is {} x {}:", multipliers.0, multipliers.1);
        println!("What is {} x {}:", question.num_1, question.num_2);

        let mut given_answer = String::new();
        io::stdin().read_line(&mut given_answer)
            .expect("Failed to read the answers.");
        question.provided_answer = match given_answer.trim()
            .parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        match question.provided_answer.cmp(question.answer.borrow()) {
            Ordering::Less => {
                print_incorrect_answer(question);
            }
            Ordering::Equal => {
                count_correct_answer = count_correct_answer + 1;
                print_correct_answer(question.provided_answer);
                question.is_correct = true;
            }
            Ordering::Greater => {
                print_incorrect_answer(question);
            }
        }
        qs_collection.qs.insert(loop_index as usize, question);
        loop_index = loop_index + 1;
    }
    if count_correct_answer.gt(&(total_questions / 2)) {
        println!("{}",
                 format!("Nice work!").bold().green()
        );
    } else {
        println!("{}",
                 format!("Good luck next time! Practice more..").red().bold()
        );
    }
    println!("You got {} correct answers", count_correct_answer);

}

fn print_correct_answer(given_answer: u32) {
    println!(
        "{}",
        format!(
            "{} is the correct answer. Good job!",
            given_answer
        )
            .bold()
            .green());
}

fn print_incorrect_answer(q: Question) {
    println!(
        "{} is incorrect. {} x {} is {}. Better luck next time.",
        format!(
            "{}",
            q.provided_answer
        )
            .bold()
            .red(),
        q.num_1,
        q.num_2,
        format!(
            "{}",
            q.answer
        )
            .bold()
            .green()
    );
}

fn genetate_question() -> Question {

    let num_1 = rand::thread_rng()
        .gen_range(1..12);
    let num_2=  rand::thread_rng()
        .gen_range(1..12);

    Question {
        num_1: num_1,
        num_2: num_2,
        answer: num_1 * num_2,
        provided_answer: 0,
        is_correct: false
    }
}

// fn generate_multipliers() -> (u32, u32, u32) {
//     let n_1: u32 = rand::thread_rng()
//         .gen_range(1..12);
//     let n_2: u32 = rand::thread_rng()
//         .gen_range(1..12);
//     (n_1, n_2, n_1 * n_2)
// }

fn print_line(length: i32) -> String {
    let mut out = "".to_owned();
    for _i in 0..length {
        out.push_str("-");
    }
    out
}